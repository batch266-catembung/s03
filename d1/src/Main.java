import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        String looptext;

    do{

        try {
            System.out.print("input an integer: ");
            int num = input.nextInt();

            long factorial = 1;
            for(int i = 1; i <= num; ++i)
            {
                factorial = factorial * i;
            }
            System.out.println("the factorial of "+ num + " is "+ factorial);

        }catch (InputMismatchException e){
            System.out.println("number only hooman");
        }catch (Exception e) {
            System.out.println("something went wrong please try again");
        }
        System.out.print("Enter Y or y to repeat the process: ");
        looptext = input.next();
    }while (looptext.equalsIgnoreCase("Y"));



    }
}
