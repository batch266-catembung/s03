package com.zuitt.example;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args) {
//        exception
        /*
        * a problem that arises during the execution of the
        * program
        *
        * it disrupts the normal flow of the program and
        * terminates abnormal
        * */

//        exception handling
        /*
        * refers to managing and catching run-time errors
        * in order to safetly run your code
        * */


        Scanner input = new Scanner(System.in);

        int num1 = 0;
        int num2 = 0;
        System.out.print("enter a number: ");

//        try-catch-finally
        try{
            num1 = input.nextInt();
            num2 = input.nextInt();
//        }catch (Exception e){
//            System.out.println("invalid input");
//            e.printStackTrace(); //display the error
//        }
            System.out.println(num1/num2);
        }catch (ArithmeticException e){
            System.out.println("you cannot divide 0 a whole number");
        }catch (InputMismatchException e){
            System.out.println("please input numbers only");
        }catch (Exception e){
            System.out.println("something went wrong please try again");
        }finally{
            System.out.println("this will execute no matter what");
        }








    }
}
