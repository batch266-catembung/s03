package com.zuitt.example;

public class LoopingStatements {
    public static void main(String[] args) {
//        loops are control structure that allows code block to be executed multiple times

//        while loop
        /*
        * Allow repetitive use of code similar to for-loops
        * but usually used for situation where the content
        * to iterate through ios indefinite.
        */
/*
* increment (++) this will add one to the variable
* decrement (--) this will subtract to the variable
* */
//        int x = 0;
//        System.out.println("while loop");
//        while (x < 10){
//            System.out.print(x+", ");
//            x++; //9
//        }
//        System.out.println("");
//        System.out.println("");
//      do while loop
/*
* similar to while loop, however do-while loop
* will execute at once
 */
//        int y = 10;
//        System.out.println("do while loop");
//        do {
//            System.out.print(y+", ");
//            y--;
//        }while(y >= 0);
//        System.out.println("");
//        System.out.println("");

//        for loop
        /*
        * for(initialValue; condition; iteration){ //code blocks }
        * */

//        System.out.println("for loop");
//        for(int i=0; i<10; i++){
//            System.out.print(i+", ");
//        };
//        System.out.println("");
//        System.out.println("");

        String[][] classroom = new String[3][3];
        //First row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";
        //Second row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "JunJun";
        classroom[1][2] = "Jobert";
        //Third row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        for(int row=0; row<3 ;row++){
            for (int col=0; col<3; col++){
                System.out.println(classroom[row][col]);
            };
        };


    }
}
